package roboSoc.Core;

import static org.junit.Assert.*;

import org.junit.Test;

public class RobotTEST {

	@Test
	public void testRobot() {
		Robot r = new Robot();
		assertTrue (r!=null && r.get_posX()==0 && r.get_posY()==0 && r.get_posZ()==0 && r.get_rotacion()==0);
	}

	@Test
	public void testRobotDoubleDoubleDouble() {
		Robot r = new Robot(1,1,1);
		assertTrue (r!=null && r.get_posX()==1 && r.get_posY()==1 && r.get_posZ()==1 && r.get_rotacion()==0);
	}

	@Test
	public void testRobotDoubleDoubleDoubleDouble() {
		Robot r = new Robot(1,0,1,0);
		assertTrue (r!=null && r.get_posX()==1 && r.get_posY()==0 && r.get_posZ()==1 && r.get_rotacion()==0);	
	}

	@Test
	public void testGet_posX() {
		Robot r = new Robot(1,0,2);
		assertTrue(r.get_posX()==1);
	}

	@Test
	public void testSet_posX() {
		Robot r = new Robot(1,0,2);
		r.set_posX(5);
		assertTrue(r.get_posX()==5);	
	}

	@Test
	public void testGet_posY() {
		Robot r = new Robot(1,0,2);
		assertTrue(r.get_posY()==0);	}

	@Test
	public void testSet_posY() {
		Robot r = new Robot(1,0,2);
		r.set_posY(11);
		assertTrue(r.get_posY()==11);	
	}

	@Test
	public void testGet_posZ() {
		Robot r = new Robot(1,0,2);
		assertTrue(r.get_posZ()==2);	
	}

	@Test
	public void testSet_posZ() {
		Robot r = new Robot(1,0,2);
		r.set_posZ(0);
		assertTrue(r.get_posZ()==0);	
	}

	@Test
	public void testGet_rotacion() {
		Robot r = new Robot(1,0,2,0.2);
		assertTrue(r.get_rotacion()==0.2);	
	}

	@Test
	public void testSet_rotacion() {
		Robot r = new Robot(1,0,2);
		r.set_rotacion(0.2);
		assertTrue(r.get_rotacion()==0.2);	
	}

	@Test
	public void testSet_Id() {
		Robot r = new Robot(1,0,2);
		r.set_Id(10);
		assertTrue(r.get_Id()==10);	
	}
	
	@Test
	public void testGet_Id() {
		Robot r = new Robot(1,0,2);
		assertTrue(r.get_Id()==0);	
	}

}
