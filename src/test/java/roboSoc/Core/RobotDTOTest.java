package roboSoc.Core;

import static org.junit.Assert.*;

import org.junit.Test;

public class RobotDTOTest {
	RobotDTO _robot;
	
	@Test
	public void testRobotDTODoubleDoubleDoubleDouble() {
		this._robot= new RobotDTO(1,1,1);
		assertTrue(this._robot.get_posX()==1 
				&& this._robot.get_posY()==1
				&& this._robot.get_posZ()==1);
	}

	@Test
	public void testRobotDTO() {
		this._robot= new RobotDTO();
		assertTrue(this._robot.get_posX()==0
				&& this._robot.get_posY()==0
				&& this._robot.get_posZ()==0);
	}

	@Test
	public void testGet_posX() {
		this._robot= new RobotDTO(1,1,1);
		assertTrue(this._robot.get_posX()==1);
	}

	@Test
	public void testSet_posX() {
		this._robot= new RobotDTO(1,1,1);
		this._robot.set_posX(2);
		assertTrue(this._robot.get_posX()==2);

	}

	@Test
	public void testGet_posY() {
		this._robot= new RobotDTO(1,1,1);
		assertTrue(this._robot.get_posY()==1);
	}

	@Test
	public void testSet_posY() {
		this._robot= new RobotDTO(1,1,1);
		this._robot.set_posY(2);
		assertTrue(this._robot.get_posY()==2);	
	}

	@Test
	public void testGet_posZ() {
		this._robot= new RobotDTO(1,1,1);
		assertTrue(this._robot.get_posZ()==1);
	}

	@Test
	public void testSet_posZ() {
		this._robot= new RobotDTO(1,1,1);
		this._robot.set_posZ(2);
		assertTrue(this._robot.get_posZ()==2);	
	}


}
