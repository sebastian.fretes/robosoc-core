package roboSoc.Core;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class PartidoTEST {

	Partido _partido;
	
	class MockPartido extends Partido{
		
		public MockPartido()
		{
			super();
			
		}
	}
	
	class TestObservador implements IObservador{
		boolean _actualizado=false;
		@Override
		public void actualizar() {this._actualizado=true;}
		public boolean actualizado() {return this._actualizado;}
	}
	
	class TestJugador implements IJugador
	{

		@Override
		public double get_posX() {return 0;}

		@Override
		public void set_posX(double _posX) {}

		@Override
		public double get_posY() {return 0;}

		@Override
		public void set_posY(double _posY) {}

		@Override
		public double get_posZ() {return 0;}

		@Override
		public void set_posZ(double _posz) {}

		@Override
		public int get_Id() {return 1;}	
	}
	
	class TestPelota implements IPosicionable
	{

		@Override
		public double get_posX() {return 0;}

		@Override
		public void set_posX(double _posX) {}

		@Override
		public double get_posY() {return 0;}

		@Override
		public void set_posY(double _posY) {}

		@Override
		public double get_posZ() {return 0;}

		@Override
		public void set_posZ(double _posz) {}


	}
	
	@Test
	public void testPartido() {
		this._partido=new MockPartido();
		assertTrue(this._partido!=null);
	}

	@Test
	public void testAgregarObservador() {
		this._partido=new MockPartido();
		TestObservador obs=new TestObservador();
		
		this._partido.agregarObservador(obs);
		this._partido.notificar();
		
		assertTrue(obs.actualizado());
	}

	@Test
	public void testQuitarObservador() {
		this._partido=new MockPartido();
		TestObservador obs=new TestObservador();
		
		this._partido.agregarObservador(obs);
		this._partido.quitarObservador(obs);
		this._partido.notificar();
		
		assertTrue(!obs.actualizado());
	}

	@Test
	public void testNotificar() {
		this._partido=new MockPartido();
		TestObservador obs=new TestObservador();
		
		this._partido.agregarObservador(obs);
		this._partido.notificar();
		
		assertTrue(obs.actualizado());
	}


	@Test
	public void testGet_equipoVisitante() {
		//fail("Not yet implemented");
		this._partido=new MockPartido();
		
		ArrayList<IJugador> equipo=new ArrayList<IJugador>();
		TestJugador j = new TestJugador();
		equipo.add(j);
		
		this._partido.set_equipoVisitante(equipo);
		
		assertTrue(this._partido.get_equipoVisitante()==equipo);
	}

	@Test
	public void testSet_equipoVisitante() {
		//fail("Not yet implemented");
		this._partido=new MockPartido();
		
		ArrayList<IJugador> equipo=new ArrayList<IJugador>();
		TestJugador j = new TestJugador();
		equipo.add(j);
		
		this._partido.set_equipoVisitante(equipo);
		
		assertTrue(this._partido.get_equipoVisitante()!=null);
	}

	@Test
	public void testGet_equipoLocal() {
		//fail("Not yet implemented");
		this._partido=new MockPartido();
		
		ArrayList<IJugador> equipo=new ArrayList<IJugador>();
		TestJugador j = new TestJugador();
		equipo.add(j);
		
		this._partido.set_equipoLocal(equipo);
		
		assertTrue(this._partido.get_equipoLocal()==equipo);
	}

	@Test
	public void testSet_equipoLocal() {
		//fail("Not yet implemented");
		this._partido=new MockPartido();
		
		ArrayList<IJugador> equipo=new ArrayList<IJugador>();
		TestJugador j = new TestJugador();
		equipo.add(j);
		
		this._partido.set_equipoLocal(equipo);
		
		assertTrue(this._partido.get_equipoLocal()!=null);
	}
	
	@Test
	public void testSetPelota()
	{
		this._partido=new MockPartido();
		
		TestPelota pelota=new TestPelota();
		
		this._partido.set_pelota(pelota);
		
		assertTrue(this._partido.get_pelota()!=null);
	}

	@Test
	public void testGetPelota()
	{
		this._partido=new MockPartido();
		
		TestPelota pelota=new TestPelota();
		
		this._partido.set_pelota(pelota);
		
		assertTrue(this._partido.get_pelota().get_posX()==0
				&&this._partido.get_pelota().get_posY()==0
				&&this._partido.get_pelota().get_posZ()==0);
	}
	
}
