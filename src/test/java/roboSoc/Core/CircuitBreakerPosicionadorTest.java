package roboSoc.Core;

import static org.junit.Assert.*;

import org.junit.Test;

public class CircuitBreakerPosicionadorTest {

	private class Servicio1 implements IServicioRemoto
	{
		int _solicitudes;
		public Servicio1() {
			super();
			this._solicitudes = 0;
		}
		@Override
		public Object solicitud(Object param) throws Exception {
			this._solicitudes++;
			return "Servicio OK.";
		}
		
	}
	
	private class Servicio2 implements IServicioRemoto
	{
		int _solicitudes;
		public Servicio2() {
			super();
			this._solicitudes = 0;
		}
		@Override
		public Object solicitud(Object param) throws Exception {
			this._solicitudes++;
					
			if(this._solicitudes>1 && this._solicitudes<=2)
			{
				Exception e = new Exception(" Error Solicitud " + (this._solicitudes));
				throw e;
			}
			
			return "Servicio OK.";
				
		}
		
	}
	
	private class Servicio3 implements IServicioRemoto
	{
		int _solicitudes;
		public Servicio3() {
			super();
			this._solicitudes = 0;
		}
		@Override
		public Object solicitud(Object param) throws Exception {
			this._solicitudes++;
					
			if(this._solicitudes<3)
			{
				Exception e = new Exception(" Error Solicitud " + (this._solicitudes));
				throw e;
			}
			
			return "Servicio OK.";
				
		}
		
	}
	
	private class Servicio4 implements IServicioRemoto
	{
		int _solicitudes;
		public Servicio4() {
			super();
			this._solicitudes = 0;
		}
		@Override
		public Object solicitud(Object param) throws Exception {
			this._solicitudes++;
					
			if(this._solicitudes>0)
			{
				Exception e = new Exception(" Error Solicitud " + (this._solicitudes));
				throw e;
			}
			
			return "Servicio OK.";
				
		}
		
	}
	
	@Test
	public void testCircuitBreakerPosicionador() {
		Servicio1 s1=new Servicio1() ;
		
		CircuitBreakerPosicionador cb=new CircuitBreakerPosicionador(s1, 1, 2);
		String respuesta = "";
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			assertTrue(false);
		}
		
		assertTrue(cb.ultimoError()=="" && respuesta=="Servicio OK.");
		
	}

	@Test
	public void testInvocarServicioError() {
		Servicio3 s3=new Servicio3() ;
		
		CircuitBreakerPosicionador cb=new CircuitBreakerPosicionador(s3, 1, 2);
		
		String respuesta = "";
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			assertTrue(true);
		}
		
		assertTrue(cb.ultimoError()!="");
	}
	
	@Test
	public void testInvocarServicioError2() {
		Servicio3 s3=new Servicio3() ;
		
		CircuitBreakerPosicionador cb=new CircuitBreakerPosicionador(s3, 1, 2);
		
		String respuesta = "";
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		assertTrue(cb.ultimoError()!="");
	}

	@Test
	public void testInvocarServicioReestablecido() {
		Servicio3 s3=new Servicio3() ;
		
		CircuitBreakerPosicionador cb=new CircuitBreakerPosicionador(s3, 1, 2);
		
		String respuesta = "";
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		//System.out.println("time : " +  System.nanoTime());
		
		/*Espero un segundo*/
		try{ 
			Thread.sleep((long) (1000)); 
		} catch(InterruptedException e ) {}; 
		
		//System.out.println("time : " +  System.nanoTime());
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		//System.out.println("respuesta : " +  respuesta);
		//System.out.println("ultimo : " +  cb.ultimoError());
		
		assertTrue(cb.ultimoError()=="" && respuesta=="Servicio OK.");
	}
	
	
	@Test
	public void testInvocarServicioReestablecidoErr() {
		Servicio4 s4=new Servicio4() ;
		
		CircuitBreakerPosicionador cb=new CircuitBreakerPosicionador(s4, 1, 2);
		
		String respuesta = "";
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		//System.out.println("time : " +  System.nanoTime());
		
		/*Espero un segundo*/
		try{ 
			Thread.sleep((long) (1000)); 
		} catch(InterruptedException e ) {}; 
		
		//System.out.println("time : " +  System.nanoTime());
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		//System.out.println("respuesta : " +  respuesta);
		//System.out.println("ultimo : " +  cb.ultimoError());
		
		assertTrue(cb.ultimoError()!="" && respuesta!="Servicio OK.");
	}
	
	
	
	
	
	@Test
	public void testUltimoError() {
		Servicio3 s3=new Servicio3() ;
		CircuitBreakerPosicionador cb=new CircuitBreakerPosicionador(s3, 1, 2);
		
		String respuesta = "";
		
		try {
			respuesta = (String) cb.invocarServicio("");
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		
		assertTrue(cb.ultimoError()!="");
	}

}
