package roboSoc.Core;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

public class Test_US1 {

	@Test
	public void testCriterioDeAceptacion1() {
	
		BuscadorComponentesJAR buscador=new BuscadorComponentesJAR("test/robosoc-pos-1.jar");
		LinkedList<Class> posicionadores;
		
		posicionadores= (LinkedList<Class>) buscador.buscarComponente("roboSoc.Core.IPosicionador");
		
		assertTrue(posicionadores.size()==0);
		
	}

	@Test
	public void testCriterioDeAceptacion2() {
	
		BuscadorComponentesJAR buscador=new BuscadorComponentesJAR("test/robosoc-pos-2.jar");
		LinkedList<Class> posicionadores;
		
		posicionadores= (LinkedList<Class>) buscador.buscarComponente("roboSoc.Core.IPosicionador");
		
		assertTrue(posicionadores.size()==0);	
	}

	@Test
	public void testCriterioDeAceptacion3() {
	
		BuscadorComponentesJAR buscador=new BuscadorComponentesJAR("test/robosoc-pos-3.jar");
		LinkedList<Class> posicionadores;
		
		posicionadores= (LinkedList<Class>) buscador.buscarComponente("roboSoc.Core.IPosicionador");
		
		IGeneradorComponentes generaComp = new GeneradorComponentesPartido(buscador);
		
		IPosicionador p = (IPosicionador) generaComp.generarInstancia(posicionadores.get(0).getName());
		
		IPosicionable p1 = p.getPosicion(1);
		IPosicionable p2 = p.getPosicion(1);
		
		assertTrue(posicionadores.get(0).getName()=="roboSoc.Pos.PosicionadorRandom" & posicionadores.size()==1 
				&& (p1.get_posX()!=p2.get_posX() || p1.get_posY()!=p2.get_posY()));	
	}
	
	@Test
	public void testCriterioDeAceptacion4() {
	
		BuscadorComponentesJAR buscador=new BuscadorComponentesJAR("test/robosoc-pos-4.jar");
		LinkedList<Class> posicionadores;
		
		posicionadores= (LinkedList<Class>) buscador.buscarComponente("roboSoc.Core.IPosicionador");
		
		if(posicionadores.size()==2)
		{
			assertTrue((posicionadores.get(0).getName()=="roboSoc.Pos.PosicionadorRandom" ||posicionadores.get(1).getName()=="roboSoc.Pos.PosicionadorRandom") 
						&& (posicionadores.get(0).getName()=="roboSoc.Pos.PosicionadorTXT" ||posicionadores.get(1).getName()=="roboSoc.Pos.PosicionadorTXT")); 
		}
		else
			assertTrue(false);	
	}


}
