package roboSoc.Core;

public interface IObservable {
	
	void agregarObservador(IObservador obs);
	void quitarObservador(IObservador obs);
	void notificar();
	
}
