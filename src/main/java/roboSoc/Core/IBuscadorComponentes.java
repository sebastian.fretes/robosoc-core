package roboSoc.Core;

import java.util.List;

public interface IBuscadorComponentes {

	List<Class> buscarComponente(String string);

}