package roboSoc.Core;

public class Robot implements IJugador{

	private double _posX;
	private double _posY;
	private double _posZ;

	double _rotacion;

	int _Nro;
	
	public Robot()
	{
		this.set_posX(0);
		this.set_posY(0);
		this.set_posZ(0);
		
		this.set_rotacion(0);
	}

	public Robot(double x, double y, double z)
	{
		this.set_posX(x);
		this.set_posY(y);
		this.set_posZ(z);
		
		this.set_rotacion(0);
	}
	
	public Robot(double x, double y, double z, double rot)
	{
		this.set_posX(x);
		this.set_posY(y);
		this.set_posZ(z);
		
		this.set_rotacion(rot);
	}
	
	
	
	
	
	@Override
	public double get_posX() {
		return this._posX;
	}

	@Override
	public void set_posX(double _posX) {
		this._posX=_posX;
	}

	@Override
	public double get_posY() {
		return this._posY;
	}

	@Override
	public void set_posY(double _posY) {
		this._posY=_posY;
	}

	@Override
	public double get_posZ() {
		return this._posZ;
	}

	@Override
	public void set_posZ(double _posZ) {
		this._posZ=_posZ;
	}
	
	
	public double get_rotacion() {
		return _rotacion;
	}

	public void set_rotacion(double _rotacion) {
		this._rotacion = _rotacion;
	}

	public void set_Id(int id)	
	{
		this._Nro=id;
	}
	
	@Override
	public int get_Id() {
		return this._Nro;
	}

}
